module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    "project": "./tsconfig.json",
    'ecmaVersion': 2019,
  },
  plugins: [
    '@typescript-eslint',
    'jest',
  ],
  extends: [
    'airbnb-typescript/base',
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:jest/recommended',
    'plugin:node/recommended',
  ],
};
